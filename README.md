# crawlLearn

#### 介绍

java 爬虫入门

网络爬虫是做什么的?    
他的主要工作就是发送请求,获取响应,解析页面,处理数据(一方面从响应中查找出想要的数据,另一方面从响应中解析出新的URL路径),然后继续访问,继续解析,继续处理,如此循环执行,直到想要的数据处理完或出现异常.

这就是网络爬虫主要做的工作. 下面是爬虫工作流程图:
![爬虫工作流程图.png](爬虫工作流程图.png)

通过上面的流程图 能大概了解到 网络爬虫 干了哪些活 ,根据这些 也就能设计出一个简单的网络爬虫出来.

一个简单的爬虫 必需的功能:

- 1: 发送请求和获取响应 Requests类 如: Page page = Requests.request(url);
- 2: 解析页面元素 PageParserUtils类 如: Elements es = PageParserUtils.select(page, "a");
- 3: 存储符合需求的数据 Files类 如: Files.saveToLocal(page);
- 4: 处理URL路径 Links类 如: Links.addUnvisitedUrlQueue(seeds);

代码结构图:
![代码结构图.png](代码结构图.png)

程序运行结果图:
![程序运行结果.png](程序运行结果.png)

`

`