package com.etoak.crawl;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sam
 */
public class PageParserUtils {


    /**
     * 通过选择器来选取页面的
     *
     * @param page        page
     * @param cssSelector cssSelector
     * @return Elements
     */
    public static Elements select(Page page, String cssSelector) {
        return page.getDoc().select(cssSelector);
    }

    /**
     * 获取满足选择器的元素中的链接 选择器cssSelector必须定位到具体的超链接
     * 例如我们想抽取id为content的div中的所有超链接，这里
     * 就要将cssSelector定义为div[id=content] a
     * 放入set 中 防止重复；
     *
     * @param cssSelector
     * @return
     */
    public static Set<String> getLinks(Page page, String cssSelector) {
        Set<String> links = new HashSet<>();
        Elements es = select(page, cssSelector);
        for (Element element : es) {
            if (element.hasAttr("href")) {
                String hrefAttr = element.attr("abs:href");
                links.add(hrefAttr);
            } else if (element.hasAttr("src")) {
                String srcAttr = element.attr("abs:src");
                links.add(srcAttr);
            }
        }
        return links;
    }


}
