package com.etoak.crawl;


import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * @author sam
 * FileUtils : 下载那些已经访问过的文件
 */
public class Files {

    private static String dirPath;


    /**
     * getMethod.getResponseHeader("Content-Type").getValue()
     * 根据 URL 和网页类型生成需要保存的网页的文件名，去除 URL 中的非文件名字符
     */
    private static String getFileName(String url, String contentType) {
        //去除 http:// 或 https
        url = url.replaceAll("^(http://|https://)", "");
        //text/html 类型
        if (contentType.contains("html")) {
            url = url.replaceAll("[?/:*|<>\"]", "_") + ".html";
            return url;
        }
        //如 application/pdf 类型
        else {
            return url.replaceAll("[?/:*|<>\"]", "_") + "." +
                    contentType.substring(contentType.lastIndexOf("/") + 1);
        }
    }


    /**
     * 生成目录
     */
    private static void mkdir() {
        if (dirPath == null) {
            URL resource = Class.class.getResource("/");
            assert resource != null;
            dirPath = resource.getPath() + "temp\\";
        }
        File fileDir = new File(dirPath);
        if (!fileDir.exists()) {
            fileDir.mkdir();
        }
    }

    /**
     * 将页面保存到本地，filePath 为要保存的文件的相对地址
     */

    public static void saveToLocal(Page page) {
        mkdir();
        String fileName = getFileName(page.getUrl(), page.getContentType());
        String filePath = dirPath + fileName;
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(filePath))) {
            byte[] data = page.getContent();
            out.write(data);
            out.flush();
            out.close();
            System.out.println("Files: 文件 " + fileName + "已经被存储在" + filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
