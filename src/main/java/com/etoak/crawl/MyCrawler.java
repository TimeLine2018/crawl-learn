package com.etoak.crawl;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Set;

/**
 * @author sam
 */
public class MyCrawler {


    /**
     * 抓取过程
     *
     * @param seeds
     * @return
     */
    public void crawling(String[] seeds) {
        //使用种子初始化 URL 队列
        System.out.println("MyCrawler: 使用种子初始化 URL 队列, 程序开始.");
        Links.addUnvisitedUrlQueue(seeds);
        //循环条件：是否 还有有效的待访问链接
        while (Links.hasUnVisitedUrlList()) {
            //先从待访问的序列中取出第一个
            String url = Links.fetchHeadOfUnVisitedUrlQueue();
            //将已经访问过的链接放入已访问的链接中；
            Links.addVisitedUrlSet(url);
            //根据URL得到page
            Page page = Requests.request(url);
            //对page进行解析：---- 解析DOM的某个标签
            System.out.println("PageParserUtils: 解析页面出页面中所有的 a 标签");
            Elements es = PageParserUtils.select(page, "a");
            es.stream().map(Element::toString).forEach(System.out::println);
            //对page进行解析：---- 得到新的链接
            System.out.println("PageParserUtils: 解析出所有的 img 标签, 并得到 新链接 ");
            Set<String> links = PageParserUtils.getLinks(page, "img");
            // Links管理类 新增链接
            System.out.println("Links: 可以将解析出来的链接 添加到 待访问链接队列中 ");
            links.stream().filter(link -> link.startsWith("http://www.baidu.com")).forEach(Links::addUnvisitedUrlQueue);
            //对page进行处理： 将保存文件
            System.out.println("Files: 将页面保存到本地");
            Files.saveToLocal(page);
        }
        System.out.println("MyCrawler: 所有的有效链接已访问结束, 程序退出.");
    }


    public static void main(String[] args) {
        MyCrawler crawler = new MyCrawler();
        crawler.crawling(new String[]{"http://www.baidu.com"});
    }

}
